

const jwt = require("jsonwebtoken");

const User = require("../model/userModel");
const requireSigning = async (req,res,next) =>{
    try{
        const decoded = jwt.verify(req.headers.authorization,process.env.JWT_SECRET);
        req.user = decoded;
        next()
    }catch(e){
        if(e.name === 'TokenExpireError'){
            res.status(401).json({msg:'Token has expired'});
        }else if(e.name === 'JsonWebTokenError'){
            res.status(403).json({msg:"Invalid token"})
        }else{
            console.log("Error verifying JWT:",e);
            res.status(500).json({msg:"Internal Server Error"});
        }
    }
}

const isAdmin = async (req,res,next) =>{
    try{
        const user = User.findById(req.user._id);
        if(user.role != 1){
            return res.status(401).json({msg:"Unauthorized"})
        }else{
            next()
        }

    }catch(e){
        console.log("catch error",e)
        res.status(500).json({msg:e.message})
    }
}

module.exports = {requireSigning,isAdmin}